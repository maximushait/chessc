/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include "Board.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::string;
void print_Board(string board);
int GetCurrentPlaceRaw(string place);
int GetCurrentPlaceColum(string place);
int GetPlaceToMoveRaw(string place);
int GetPlaceToMoveColum(string place);
void main()
{
	srand(time_t(NULL));
	
	system("Start chessGraphics.exe");
	Sleep(1000);
	
	
	//print_Board("rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1");


	Board board = Board("rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0");
	board.print_Board();

	int returnNumber = 0;
	int currentPlaceRaw = 0;
	int currentPlaceColum = 0;
	int  placeToMoveRaw = 0;
	int placeToMoveColum = 0;

	Pipe p;
	bool isConnect = p.connect();
	
	char ans[2];

	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE






	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"); // just example...
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	//printing the msg
	cout << msgFromGraphics << endl;
	
	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		
		//like  getting e4
		currentPlaceRaw = GetCurrentPlaceRaw(msgFromGraphics);// like getting	e
		currentPlaceColum = GetCurrentPlaceColum(msgFromGraphics);// like getting	2
		placeToMoveRaw = GetPlaceToMoveRaw(msgFromGraphics);//like  getting e
		placeToMoveColum = GetPlaceToMoveColum(msgFromGraphics);//like  getting 4



		//move the player in the board
		//function from the board


		// YOUR CODE

		//getting the number from the move is it legal or not
		returnNumber = board.MovePlayerInBoardValid(currentPlaceRaw, currentPlaceColum, placeToMoveRaw, placeToMoveColum);
		ans[0] = (returnNumber + '0');
		ans[1] = NULL;
		strcpy_s(msgToGraphics, ans); // msgToGraphics should contain the result of the operation


		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   
		board.print_Board();//printing in the console
		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}
void print_Board(string board)
{
	for (int i = 0; i < 64; i++)
	{
		if (i % 8 == 0 && i != 0)
		{
			cout << endl;
		}
		cout << board[i] << " ";
	}
	cout << endl;
	cout << board[64] << endl;
	if (board[64] == '0')
	{
		cout << "Current player: White" << endl;
	}
	else
	{
		cout << "Current player: Black" << endl;
	}
	
}
int GetCurrentPlaceRaw(string place)
{
	return '8' - place[1];
}
int GetCurrentPlaceColum(string place)
{
	return place[0] - 'a';
}
int GetPlaceToMoveRaw(string place)
{
	return '8' - place[3];
}
int GetPlaceToMoveColum(string place)
{
	return place[2] - 'a';
}