#pragma once
#include "Player.h"
#define DIAGONAL 1.0
class Bishops: public Player
{
public:
	Bishops(const int raw, const int colum, const char color, Board* board);
	~Bishops();
	virtual int CheckCanMoveTo(const int raw, const int colum);
};