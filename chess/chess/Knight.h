#pragma once
#include "Player.h"
class  Knight: public Player
{
public:
	 Knight(const int raw, const int colum, const char color, Board* board);
	~Knight();
	virtual int CheckCanMoveTo(const int raw, const int colum);
};
