#pragma once
#include <string>
#include "Board.h"
using namespace std;
class Board;
class Player {
protected:
	int _raw;
	int _colum;
	char _color;
	Board* _board;


public:
	//constructors and distructors
	Player(const int raw, const int colum, const char color, Board* board);
	~Player();



	//getting the letter of the color
	char GetColorLetter()const;
	//getting White/Black
	string GetColor()const;
	int GetColorNumber()const;


	//get the place as string
	string GetCurrentPlace() const;




	virtual void ChangePlace(const int raw, const int colum);

	virtual int CheckCanMoveTo(const int raw, const int colum) = 0;

	int GetRawNumber();
	int GetColumNumber();
};