#pragma once
#define WHITE_MOVE 1
#define BLACK_MOVE -1 
#include "Player.h"
class Pawns : public Player
{
private:
	bool _DidFirstMove;


public:
	Pawns(const int raw, const int colum, const char color, Board * board);
	~Pawns();

	virtual int CheckCanMoveTo(const int raw, const int colum);
};