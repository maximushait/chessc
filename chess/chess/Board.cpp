#include "Board.h"
#include "Pawns.h"
#include "Rook.h"
#include "Bishops.h"
#include "Knight.h"
#include "King.h"
#include "Queen.h"
#include <iostream>
#include <math.h>
using namespace std;
Board::Board(const string board)
{
	int count = 0;
	int number = 0;
	//rnbkqbnr pppppppp ######## ######## ######## ######## PPPPPPPP RNBKQBNR
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if (board[count] == 'p')
			{
				_board[i][j] = new Pawns(i, j, 'p', this);
			}
			else if (board[count] == 'P')
			{
				_board[i][j] = new Pawns(i, j, 'P', this);
			}
			else if (board[count] == 'r')
			{
				_board[i][j] = new Rook(i, j, 'r', this);
			}
			else if (board[count] == 'R')
			{
				_board[i][j] = new Rook(i, j, 'R', this);
			}
			else if (board[count] == 'k')
			{
				_board[i][j] = new King(i, j, 'k', this);
				_kingsPosition[BLACK] = _board[i][j];
			}
			else if (board[count] == 'K')
			{
				_board[i][j] = new King(i, j, 'K', this);
				_kingsPosition[WHITE] = _board[i][j];
			}
			else if (board[count] == 'n')
			{
				_board[i][j] = new Knight(i, j, 'n', this);
			}
			else if (board[count] == 'N')
			{
				_board[i][j] = new Knight(i, j, 'N', this);
			}
			else if (board[count] == 'b')
			{
				_board[i][j] = new Bishops(i, j, 'b', this);
			}
			else if (board[count] == 'B')
			{
				_board[i][j] = new Bishops(i, j, 'B', this);
			}
			else if (board[count] == 'q')
			{
				_board[i][j] = new Queen(i, j, 'q', this);
			}
			else if (board[count] == 'Q')
			{
				_board[i][j] = new Queen(i, j, 'Q', this);
			}
			else if (board[count] == '#')
			{
				_board[i][j] = nullptr;
			}
			//cout << board[count] << " ";
			count++;
			
		}
		//cout << endl;
		number++;
	}
	//cout << endl << "finshed" << endl;
	_currentPlayer = board[count] - 48;
}
Board::Board(const Board& board)
{
	Player* currentPlayer = nullptr;
	for (int i = 0; i < LEN; i++)
	{
		for (int j = 0; j < LEN; j++)
		{
			currentPlayer = board.GetPlayerInPlace(i, j);
			if (currentPlayer == nullptr)
			{
				_board[i][j] = nullptr;
			}
			else if (currentPlayer->GetColorLetter() == 'p')
			{
				_board[i][j] = new Pawns(i, j, 'p', this);
			}
			else if (currentPlayer->GetColorLetter() == 'P')
			{
				_board[i][j] = new Pawns(i, j, 'P', this);
			}
			else if (currentPlayer->GetColorLetter() == 'r')
			{
				_board[i][j] = new Rook(i, j, 'r', this);
			}
			else if (currentPlayer->GetColorLetter() == 'R')
			{
				_board[i][j] = new Rook(i, j, 'R', this);
			}
			else if (currentPlayer->GetColorLetter() == 'k')
			{
				_board[i][j] = new King(i, j, 'k', this);
				_kingsPosition[BLACK] = _board[i][j];
			}
			else if (currentPlayer->GetColorLetter() == 'K')
			{
				_board[i][j] = new King(i, j, 'K', this);
				_kingsPosition[WHITE] = _board[i][j];
			}
			else if (currentPlayer->GetColorLetter() == 'n')
			{
				_board[i][j] = new Knight(i, j, 'n', this);
			}
			else if (currentPlayer->GetColorLetter() == 'N')
			{
				_board[i][j] = new Knight(i, j, 'N', this);
			}
			else if (currentPlayer->GetColorLetter() == 'b')
			{
				_board[i][j] = new Bishops(i, j, 'b', this);
			}
			else if (currentPlayer->GetColorLetter() == 'B')
			{
				_board[i][j] = new Bishops(i, j, 'B', this);
			}
			else if (currentPlayer->GetColorLetter() == 'q')
			{
				_board[i][j] = new Queen(i, j, 'q', this);
			}
			else if (currentPlayer->GetColorLetter() == 'Q')
			{
				_board[i][j] = new Queen(i, j, 'Q', this);
			}
			

		}
		
	}
	_currentPlayer = board._currentPlayer;
}
Board::~Board()
{
	int i = 0, j = 0;

	// Delete all the allocated memory for the pieces on the board
	for (i = 0; i < LEN; i++)
	{
		for (j = 0; j < LEN; j++)
		{
			if (_board[i][j] != nullptr) // Check if there is a piece on the current square
			{
				delete _board[i][j];
			}
		}
	}
}
void Board::print_Board()
{
	Player* p = nullptr;
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			p = _board[i][j];
			if ( p == nullptr)
			{
				cout << '#' << " ";
			}
			else
			{
				cout << p->GetColorLetter() << " ";
			}
		}
		cout << endl;
	}
	cout <<endl << "current player: " << _currentPlayer << endl;
}
Player* Board::GetPlayerInPlace(const int raw, const int colum) const
{
	return _board[raw][colum];
}
void Board::MovePlayer(Player& player, const int raw, const int colum)
{
	if (_board[raw][colum] != nullptr)
	{
		delete _board[raw][colum]; // Free memory of the piece on the square
	}
	_board[player.GetRawNumber()][player.GetColumNumber()] = nullptr;
	_board[raw][colum] = &player;
	player.ChangePlace(raw, colum);//changing the place in the player object
}
int Board::MovePlayerInBoardValid(const int currentraw, const int currentcolum, const int rawToMove, const int columToMove)
{
	Player* player = GetPlayerInPlace(currentraw, currentcolum);//the player in the place
	Board* copyBoard = nullptr;

	int returnCode = 0;
	bool currentplayer = true;
	//checking for the current player
	if (player->GetColor() == "White" && _currentPlayer == BLACK)
	{
		currentplayer = false;
	}
	else if(player->GetColor() == "Black" && _currentPlayer == WHITE)
	{
		currentplayer = false;
	}
	if (!currentplayer)
	{
		return INVALID_SRC;
	}
	if (player != nullptr)
	{
		if (currentcolum != columToMove || currentraw != rawToMove)
		{
			returnCode = player->CheckCanMoveTo(rawToMove, columToMove);//check if the player can move like this 

			if (returnCode == VALID)
			{

				// Check if we cause a check on ourselves!!


				copyBoard = new Board(*this);
				// Create a new board, make the move and check if we cause a check on ourself
				copyBoard->MovePlayer(*copyBoard->GetPlayerInPlace(currentraw, currentcolum), rawToMove, columToMove);//moving the player in the fake board


				//copyBoard->ChangePlayer(); // Switch the playing color


				if (copyBoard->checkCheck())
				{
					return INVALID_SELF_CHESS;//check on my self
				}

				// The piece can be moved! but we need to check if we did check on our opponent
				else
				{
					ChangePlayer(); // Switch the playing side because we are going to move our player
					MovePlayer(*GetPlayerInPlace(currentraw, currentcolum), rawToMove, columToMove); // Move the piece on the board;
					copyBoard->ChangePlayer();
					// Check for a check on the opposing side
					if (copyBoard->checkCheck())
					{
						return VALID_CHECK;
	
					}
				}
				copyBoard->~Board();///destroying the board
				return returnCode;
			}
			else
			{
				return returnCode;//if the player cant move like this then I'll return the number
			}
		}
		else
		{
			return INVALID_SAME_SQR;
		}
		
	}
	else 
	{
		return INVALID_SRC;
	}
	
}
void Board::ChangePlayer()
{
	if (_currentPlayer == WHITE)
	{
		_currentPlayer = BLACK;
	}
	else
	{
		_currentPlayer = WHITE;
	}
}
bool Board::checkCheck()
{
	Player* currPiece = nullptr;
	int checkedKing = WHITE;
	int i = 0, j = 0;
	bool isCheck = false;
	int color = WHITE;
	// Check if we need to check on the white or black side
	if (_currentPlayer == WHITE)//black player
	{
		color = BLACK; // Check white on black king
	}
	for (i = 0; i < LEN && !isCheck; i++)
	{
		for (j = 0; j < LEN && !isCheck; j++)
		{
			currPiece = GetPlayerInPlace(i, j);
			// Check if the current square isn't null, then check if the piece is in the correct color
			if (currPiece != nullptr && currPiece->GetColorNumber() == color)
			{
				// Check if the move from our piece to the king is possible
				isCheck = currPiece->CheckCanMoveTo(_kingsPosition[checkedKing]->GetRawNumber(), _kingsPosition[checkedKing]->GetColumNumber()) == VALID;
			}
		}
	}
	return isCheck;
}
int Board::GetCurrentPlayer()
{
	return _currentPlayer;
}