#include "Knight.h"
Knight::Knight(const int raw, const int colum, const char color, Board* board) :
	Player(raw, colum, color, board)
{

}
Knight::~Knight()
{

}
	/*
Checks if the move to the given square is valid (not including check).
Input: Row and column of the square
Output: The return code (valid or not and reason)
*/
int Knight::CheckCanMoveTo(const int raw, const int colum)
{
		int retCode = VALID;
		if (_board->GetPlayerInPlace(raw, colum) != nullptr && GetColor() == _board->GetPlayerInPlace(raw, colum)->GetColor())
		{
			retCode = INVALID_DEST;

		}
		// In all valid knight moves, he is moved one square in one axis and 2 squares in the other 
		if (retCode == VALID && !(abs(_colum - colum) == 2 && abs(_raw - raw) == 1 || abs(_colum - colum) == 1 && abs(_raw - raw) == 2))
		{
			retCode = INVALID_MOVE;
		}
		return retCode;
}