#include "Player.h"
#include <string>

Player::Player(const int raw, const int colum, const char color, Board * board)
{
	_raw = raw;
	_colum = colum;
	_color = color;
	_board = board;
}
Player::~Player()
{

}

char Player::GetColorLetter()const
{
	return _color;
}

string Player::GetColor() const
{
	if (_color >= 'A' && _color <= 'Z')
		return "White";
	else if(_color >= 'a' && _color <= 'z')
		return "Black";
	return "None";
}

string Player::GetCurrentPlace() const
{
	return "" + (_colum + 'a') + ('8' - _raw) + NULL;
}

int Player::GetRawNumber()
{
	return _raw;
}
int Player::GetColumNumber()
{
	return _colum;
}
//check chess
void Player::ChangePlace(const int raw, const int colum)
{
	_raw = raw;
	_colum = colum;
}
int Player::GetColorNumber()const
{
	if (_color >= 'A' && _color <= 'Z')
	{
		return 0;
	}
	else
	{
		return 1;
	}
}