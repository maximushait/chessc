#pragma once
#include "Player.h"
#define BLACK 1
#define WHITE 0
#define LEN 8
enum RET_CODES {
	VALID, VALID_CHECK, INVALID_SRC, INVALID_DEST, INVALID_SELF_CHESS,
	INVALID_INDEX, INVALID_MOVE, INVALID_SAME_SQR, VALID_CHECKMATE
};
using namespace std;
class Player;
class Board
{
private:
	Player* _board[8][8];
	int _currentPlayer;
	Player* _kingsPosition[2];

public:
	Board(const string board);
	Board(const Board& board);
	~Board();


	Player* GetPlayerInPlace(const int raw, const int colum) const;

	int MovePlayerInBoardValid(const int currentraw, const int currentcolum, const int rawToMove, const int columToMove);

	void print_Board();
	//moving the player to place
	void MovePlayer(Player& player, const int raw, const int colum);
	void ChangePlayer();
	bool checkCheck();
	int GetCurrentPlayer();
};