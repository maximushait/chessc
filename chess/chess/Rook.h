#pragma once
#include "Player.h"

class Rook : public Player
{
private:


public :
	Rook(const int raw, const int colum, const char color, Board * board);
	~Rook();
	virtual int CheckCanMoveTo(const int raw, const int colum);
};