#include "Queen.h"
Queen::Queen(const int raw, const int colum, const char color, Board* board) :
	Player(raw, colum, color, board)
{

}
Queen::~Queen()
{

}
int Queen::CheckCanMoveTo(const int raw, const int colum)
{
	int check = VALID;
	int rowIncLine = 0, colIncLine = 0;
	int currRow = _raw, currCol = _colum;





	if (_board->GetPlayerInPlace(raw, colum) != nullptr && GetColor() == _board->GetPlayerInPlace(raw, colum)->GetColor())
	{
		check = INVALID_DEST;

	}
	if (abs(_colum - colum) != abs(_raw - raw) && (_colum != colum && _raw != raw))
	{
		check = INVALID_MOVE;//invalid move code:6
	}
	else if (_colum != colum && _raw == raw)
	{
		colIncLine = (colum - _colum) / abs(colum - _colum);

		currCol += colIncLine;
		while (currCol != colum && check == VALID)
		{
			if (_board->GetPlayerInPlace(currRow, currCol) != nullptr)
			{
				check = INVALID_MOVE;
			}
			currCol += colIncLine;
		}
	}
	else if (_raw != raw && _colum == colum)
	{
		rowIncLine = (raw - _raw) / abs(raw - _raw);

		currRow += rowIncLine;
		while (currRow != raw && check == VALID)
		{
			if (_board->GetPlayerInPlace(currRow, currCol) != nullptr)
			{
				check = INVALID_MOVE;
			}
			currRow += rowIncLine;
		}
	}
	else
	{
		rowIncLine = (raw - _raw) / abs(raw - _raw);
		colIncLine = (colum - _colum) / abs(colum - _colum);

		currRow += rowIncLine;
		currCol += colIncLine;
		while (currRow != raw && currCol != colum && check == VALID)
		{
			if (_board->GetPlayerInPlace(currRow, currCol) != nullptr)
			{
				check = INVALID_MOVE;
			}
			currRow += rowIncLine;
			currCol += colIncLine;
		}
	}
	return check;
}