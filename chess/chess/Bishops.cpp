#include "Bishops.h"
Bishops::Bishops(const int raw, const int colum, const char color, Board* board) :
	Player(raw, colum, color, board)
{

}
Bishops::~Bishops()
{

}
int Bishops::CheckCanMoveTo(const int raw, const int colum)
{
	int retCode = VALID;
	int rowIncline = 0, colIncline = 0;



	int curRow = _raw, curCol = _colum;





	if (_board->GetPlayerInPlace(raw, colum) != nullptr && GetColor() == _board->GetPlayerInPlace(raw, colum)->GetColor())
	{
		retCode = INVALID_DEST;
		return retCode;

	} // Check if the destination is available



	// Diagonal movement is in a decline or incline of 1 
	if (retCode == VALID && (_raw == raw || _colum == colum || abs(((float)(_raw - raw) / (_colum - colum))) != DIAGONAL)) // The bishop can only move diagonally
	{
		retCode = INVALID_MOVE;
	}
	else
	{
		// Make the incline 1 - positive or negative
		rowIncline = (raw - _raw) / abs(raw - _raw);
		colIncline = (colum - _colum) / abs(colum - _colum);

		// Start from the next square
		curRow += rowIncline;
		curCol += colIncline;
		if (curCol == colum && curRow == raw)
		{
			if (_board->GetPlayerInPlace(curRow, curCol) != nullptr && _board->GetPlayerInPlace(curRow, curCol)->GetColor() == GetColor())
			{
				retCode = INVALID_MOVE;
			}
		}
		while (curRow != raw && curCol != colum && retCode == VALID)
		{
			if (_board->GetPlayerInPlace(curRow, curCol) != nullptr && _board->GetPlayerInPlace(curRow, curCol)->GetColor() == GetColor())
			{
				retCode = INVALID_MOVE;
			}
			curRow += rowIncline;
			curCol += colIncline;
		}
	}
	return retCode;
}