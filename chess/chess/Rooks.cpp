#include "Rook.h"
Rook::Rook(const int raw, const int colum, const char color, Board* board):
	Player(raw, colum, color, board)
{

}
Rook::~Rook()
{

}
int Rook::CheckCanMoveTo(const int raw, const int colum)
{
	
	int currentplaceRaw = GetRawNumber();
	int currentplaceColum = GetColumNumber();

	//int ToMoveplaceRaw = GetRawNumberInt(placeToMoveTo);
	//int ToMoveplaceColum = GetColumNumberInt(placeToMoveTo);
	int add = 0;
	int addRaw = _raw;
	int addColum = _colum;
	int addC = 0;



	if (_raw != raw && _colum != colum)//if not in the raw or in the colum, cant move like that
	{
		return INVALID_MOVE;
	}
	else if (_colum == colum && _raw != raw)
	{
		add = (raw - _raw) / abs(raw - _raw);

		//addRaw += add;
		while (addRaw != raw)
		{
			addRaw += add;

			if (_board->GetPlayerInPlace(addRaw, addColum) != nullptr)
			{
				return INVALID_MOVE;
			}
			
		}
	}
	else if (_colum != colum && _raw == raw)
	{
		add = (colum - _colum) / abs(colum - _colum);

		//addColum += add;
		while (addColum != colum)
		{
			addColum += add;
			if (_board->GetPlayerInPlace(addRaw, addColum) != nullptr)
			{
				return INVALID_MOVE;
			}
			
		}
	}
	return VALID;
}