#pragma once
#include "Player.h"
class Queen : public Player
{
public:
	Queen(const int raw, const int colum, const char color, Board* board);
	~Queen();
	virtual int CheckCanMoveTo(const int raw, const int colum);
};
