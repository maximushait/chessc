#pragma once
#include "Player.h"


class King: public Player
{
public:
	King(const int raw, const int colum, const char color, Board* board);
	~King();
	virtual int CheckCanMoveTo(const int raw, const int colum);
};