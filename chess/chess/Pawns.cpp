#include "Pawns.h"
#include <string>

Pawns::Pawns(const int raw, const int colum, const char color, Board * board) :
	Player(raw, colum, color, board)
{
	_DidFirstMove = false;
}
Pawns::~Pawns()
{

}

int Pawns::CheckCanMoveTo(const int raw, const int colum)
{
	int retCode = VALID;
	int rowInc = WHITE_MOVE;//black or white

	if (_board->GetCurrentPlayer() == BLACK)
	{
		rowInc = BLACK_MOVE;
	}


	if (_board->GetPlayerInPlace(raw, colum) != nullptr && GetColor() == _board->GetPlayerInPlace(raw, colum)->GetColor())
	{
		retCode = INVALID_DEST;
		return retCode;
	}
	if (retCode == VALID)
	{
		if ((!_DidFirstMove && (_raw - raw != rowInc && _raw - raw != rowInc * 2)) || (_DidFirstMove && _raw - raw != rowInc) )
		{
			retCode = INVALID_MOVE;
		}
		else if (abs(colum - _colum) != 0 &&
			!(abs(colum - _colum) == 1 && _raw - raw == rowInc && _board->GetPlayerInPlace(raw, colum) != nullptr))
		{
			retCode = INVALID_MOVE;
		}
		else if ((_board->GetPlayerInPlace(raw, colum) != nullptr && abs(colum - _colum) == 0) &&
			(!_DidFirstMove && _raw - raw == rowInc || _DidFirstMove && _raw - raw == rowInc || _raw - raw == rowInc * 2))
		{
			retCode = INVALID_MOVE;
		}
		else if (_DidFirstMove && _raw - raw == rowInc * 2 && _board->GetPlayerInPlace(_raw - rowInc, _colum) != nullptr)
		{
			retCode = INVALID_MOVE;
		}
		return retCode;
	}
	return retCode;
	//return 1;
	/*string placeTo = _place;
	if (!_DidFirstMove)
	{
		if (GetColor().compare("White") == 0)
		{
			placeTo[1] = GetRawNumber() + 1;
			if (placeTo.compare(placeToMoveTo) == 0)
			{
				return true;
			}
			placeTo[1] = GetRawNumber() + 2;
			if (placeTo.compare(placeToMoveTo) == 0)
			{
				return true;
			}
			return false;
		}
		else if (GetColor().compare("Black") == 0)
		{
			placeTo[1] = GetRawNumber() - 1;
			if (placeTo.compare(placeToMoveTo) == 0)
			{
				return true;
			}
			placeTo[1] = GetRawNumber() - 2;
			if (placeTo.compare(placeToMoveTo) == 0)
			{
				return true;
			}
			return false;
		}
	}
	else
	{
		if (GetColor().compare("White") == 0)
		{

		}
		else if (GetColor().compare("Black") == 0)
		{

		}
	}
	return false;*/
}