#include "King.h"
King::King(const int raw, const int colum, const char color, Board* board):
	Player(raw, colum, color, board)
{

}
King::~King()
{

}
int King::CheckCanMoveTo(const int raw, const int colum)
{
	int retCode = 0;
	if (_board->GetPlayerInPlace(raw, colum) != nullptr && GetColor() == _board->GetPlayerInPlace(raw, colum)->GetColor())
	{
		retCode = INVALID_DEST;

	}
	if (std::abs(_colum - colum) >= 2 || std::abs(_raw - raw) >= 2)
	{
		retCode = INVALID_MOVE;//invalid move code:6
	}
	return retCode;
}
